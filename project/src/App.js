
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';
import MenuLeft from './components/Layout/MenuLeft';
import MenuAcc from './components/Account/MenuAcc';
import { useLocation } from 'react-router-dom';
import { UserContext } from './UserContext';
import { useState } from 'react';
import Slider from './components/Layout/Slider';
function App(props) {
  let params1 = useLocation()
  const [sumQty, setSumQty] = useState(0)

  return(
    <UserContext.Provider value={{sumQty, setSumQty }}>
      <Header/>
      {params1['pathname'].includes('home') ? <Slider/> : null}
      <section>
        <div className='container'>
          {params1['pathname'].includes('account') || params1['pathname'].includes('product') || params1['pathname'].includes('addProduct') ? <MenuAcc/> : <MenuLeft/>}
          {props.children}
          
        </div>
      </section>
      <Footer/>
    </UserContext.Provider>
  )
};
export default App;
