import axios from "axios";
import { useState } from "react";
import ErrorLogin from "./ErrorLogin";
function Register(props) {
    const [inputs , setInputs] = useState({
        email:'',
        password:"",
        name:'',
        phone:'',
        address:'',
        country:'',
        level:0
    });
    const [file,setFile] = useState('')
    const [avatar,setAvatar] = useState('')
    const [error,setError] = useState({})
    function handleChange(e) {
        let valueInput = e.target.value
        let nameInput = e.target.name
        setInputs(state => ({...state,[nameInput] : valueInput}))
    }
    function handleFile(e) {
        let file = e.target.files
        if (file) {
            let reader = new FileReader();
            reader.onload = (e) =>{
                setAvatar(e.target.result);
                setFile(file)
            }
            reader.readAsDataURL(file[0])
        }
    }
    function handleTypeFile(files) {
        const arrFile = [
            'png',
            'jpg',
            'jpeg',
            'PNG',
            'JPG'
        ]
        if (files) {
            let nameFile = (files.name).split('.')
            let typeFile = nameFile[nameFile.length - 1]
            return arrFile.includes(typeFile)
        }

    }
    function handleSubmit(e) {
        // inputs.file = avatar
        // setData(state => ({...state,inputs}))
        e.preventDefault()
        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let errorSubmit = {};
        let flag = true;
        if(inputs.name === '') {
            errorSubmit.name = 'name: không được để trống'
            flag= false
        }
        if(!regex.test(inputs.email)) {
            errorSubmit.email = 'email: vui lòng nhập đúng'
            flag= false
        }
        if(inputs.email === '') {
            errorSubmit.email = 'email: không được để trống'
            flag= false
        }
        if(inputs.password === '') {
            errorSubmit.pass = 'pass: không được để trống'
            flag= false
        }
        if(inputs.phone === '') {
            errorSubmit.phone = 'phone: không được để trống'
            flag= false
        }
        if(inputs.address === '') {
            errorSubmit.address = 'address: không được để trống'
            flag= false
        }
        if(file === '') {
            errorSubmit.file = 'file: không được để trống'
            flag= false
        } else {
            if(file[0].size > 1024*1024) {
                errorSubmit.file = 'file: kích thước lớn'
                flag= false
            }
            if(!handleTypeFile(file[0])) {
                errorSubmit.file = 'file: không đúng'
                flag= false
            }
        }
        if(inputs.country === '') {
            errorSubmit.country = 'country: không được để trống'
            flag= false
        }
        if (!flag) {
            setError(errorSubmit)
        } else {
            let data = inputs
            console.log(data.password);
            data['avatar'] = avatar;
            axios.post('http://localhost/laravel/laravel/public/api/register', data)
            .then(response => {
                console.log(response.data);
                if (response.data.errors) {
                    setError(response.data.errors)
                } else {
                    alert('dk thành công')
                    setError('')
                }
            })
        }
    }
    
    return(
        <div className="col-sm-4">
            <div className="signup-form">{/*sign up form*/}
                <h2>Register!</h2>
                <ErrorLogin error={error}/>
                <form onSubmit={handleSubmit} action="#" enctype='multipart/form-data'>
                <input className="form-control" onChange={handleChange} type="name" name="name" placeholder="Full Name" />
                <input className="form-control" onChange={handleChange}  type="email" name="email" placeholder="Email Address" />
                <input className="form-control" onChange={handleChange} type="password" name="password" placeholder="Password" />
                <input className="form-control" onChange={handleChange} type="number" name="phone" placeholder="Phone" />
                <input className="form-control" onChange={handleChange} type="text" name="address" placeholder="Address" />
                <input className="form-control" name="file"  type='file' onChange={handleFile}/>
                <div >
                    <select  onChange={handleChange}  name="country" className="form-control form-control-line">
                      <option selected value>Please select</option>
                      <option  value={1}>vietnam</option>
                      <option value={2}> anh </option>
                      <option value={3}> phap</option>
                    </select>
                  </div>
                <button type="submit" className="btn btn-default">Register</button>
                </form>
            </div>{/*/sign up form*/}
        </div>
    )
};
export default Register;