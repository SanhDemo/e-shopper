import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import ErrorLogin from "./ErrorLogin";

function Login(props) {
    const [inputs , setInputs] = useState({
        email:'',
        password:"",
        level:0,
        checked:""
    });
    const navigate = useNavigate()
    const [error ,setError] = useState({})
    const [ checked,setChecked] = useState('')
    function handleChange(e) {
      let nameInput = e.target.name
      let valueInput = e.target.value
      setInputs(state =>({...state,[nameInput]:valueInput}))
    }

    function handleChecked(e) { 
      setChecked(e.target.checked)
    }
    function handleSubmit(e) {
      e.preventDefault()
      let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      let errorSubmit = {};
      let flag = true;
      if(!regex.test(inputs.email)) {
        errorSubmit.email = 'email: vui lòng nhập đúng'
        flag= false
      }
      if (inputs.email ==='') {
        errorSubmit.email = "email: không được để trống"
        flag = false
      }
      if (inputs.password ==='' ) {
        errorSubmit.password = "pass: không được để trống"
        flag = false
      }
      if (!checked) {
        errorSubmit.checked = "vui long check"
        flag = false
      }
      if (!flag) {
        setError(errorSubmit)
      } else {
        let data = inputs
        axios.post('http://localhost/laravel/laravel/public/api/login', data)
        .then(response => {
          console.log(response);
          if (response.data.errors) {
            setError(response.data.errors)
          } else {
            navigate('/home')
            let isLogin = true;
            localStorage.setItem('isLogin',JSON.stringify(isLogin))
            localStorage.setItem("data",JSON.stringify(response.data))
            setError('')
          }
        })
      }
    }
    return(
      <div className="col-sm-4 ">
            <div className="login-form">
                <h2>Login to your account</h2>
                <ErrorLogin error ={error} />
                <form onSubmit={handleSubmit} action="#">
                    <input name="email" onChange={handleChange} type="email" placeholder="Email Address" />
                    <input name='password' onChange={handleChange} type="password" placeholder="PassWord" />
                    <span>
                      <input onChange={handleChecked} name='checkbox' type="checkbox" class="checkbox" /> 
                      Keep me signed in
                    </span>
                    <button type="submit" className="btn btn-default">Login</button>
                </form>
            </div>
        </div>
           /* </div><div className="signup-form">
              <ErrorLogin/>
                <h2>New User Signup!</h2>
                <form action="#">
                <input type="email" placeholder="Email Address" />
                <input type="password" placeholder="Pass word" />
                <button type="submit" className="btn btn-default">Signup</button>
                </form>
            </div>
        </div> */
    )
};
export default Login;