import Register from "./Register";
import Login from "./Login";

function Index(props) {
    return(
        <section id="form" className="col-sm-9">
            <div class="container">
			<div class="row">
				<Login/>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<Register/>
			</div>
		</div>
        </section>
    )
};
export default Index;