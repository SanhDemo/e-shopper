import React , {useContext, useEffect, useState} from "react"
import { Link } from "react-router-dom"
import axios from "axios"
function Wishlist(props) {
    let wishlist = JSON.parse(localStorage.getItem('dataWishlist'))
    localStorage.setItem('wishlist',JSON.stringify(wishlist))
    console.log(wishlist);
    const [data,setData] = useState([])
    useEffect(()=> {
        axios.get('http://localhost/laravel/laravel/public/api/product/wishlist')
    .then(res=> {
        setData(res.data.data);
    })
    } ,[])
    
    function renderWishlist() {
      if (wishlist.length > 0) {
        return  wishlist.map((value)=> {
            return  Object.keys(data).map((key,index)=>{
                  let img = JSON.parse(data[key].image)
                  if (parseInt(value) == data[key].id) {
                      return (
                              <div key={index} className="col-sm-4">
                                  
                                  <div className="product-image-wrapper">
                                      <div className="single-products">
                                      <div className="productinfo text-center">
                                          <img src={'http://localhost/laravel/laravel/public/upload/user/product/'+ data[key].id_user +'/'+ img[0]} alt="" />
                                          <h2>${data[key].price}</h2>
                                          <p>{data[key].name}</p>
                                          <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                      </div>
                                      <div className="product-overlay">
                                          <div className="overlay-content">
                                          <h2>${data[key].price}</h2>
                                          <p>{data[key].name}</p>
                                          <a href="#" id={data[key].id}   className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                          </div>
                                      </div>
                                      </div>
                                      <div className="choose">
                                      <ul className="nav nav-pills nav-justified">
                                          <li><a id={data[key].id} onClick={removeWishlist} href="#"><i className="fa fa-plus-square" />Remove to wishlist</a></li>
                                          <li><Link to ={'/product/detail/'+ data[key].id}><i className="fa fa-plus-square" />Product detail</Link></li>
                                      </ul>
                                      </div>
                                  </div>
                              </div>
                          )
                  }
              })
          })
      }
    }
//    ham xoa yeu thich
function removeWishlist(e) {
    let id = e.target.id

 }
    return(
        <div className="col-sm-9 ms-sm-1">
            {renderWishlist()}
        </div>
    )
}
export default Wishlist