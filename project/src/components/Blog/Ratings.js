
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import StarRatings from 'react-star-ratings';

function Ratings(props) {
  let {id} = props
  const userData = JSON.parse(localStorage.getItem('data'))
  let url = 'http://localhost/laravel/laravel/public/api/blog/rate/' + id
    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ userData.success.token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
  let isLogin = JSON.parse(localStorage.getItem('isLogin'))
  const [rating, setRating] = useState(0)
  const navigate = useNavigate()
  useEffect(()=>{
    axios.get('http://localhost/laravel/laravel/public/api/blog/rate/' + id)
    .then(res=> {
      let rates = res.data.data
      let sumRate = 0;
      let star = 0;
      rates.map((value) => {
        sumRate += value.rate
        star =sumRate/rates.length
        return setRating(star)
      })
    })
    .catch(error => console.log(error))
  },[])
      function renderRate(){
        function changeRating( newRating, name ) {
          if (!isLogin) {
            alert('vui long dang nhap')
            navigate('/login')
          } else {
            setRating(newRating)
          }
        }
        if (rating != 0 ) {
          const formData = new FormData();
                    formData.append('blog_id',id)
                    formData.append('user_id', userData.Auth.id)
                    formData.append('rate', rating)
                    axios.post(url,formData,config)
                    .then(response => {
                    })
        }
          return (
            <StarRatings
              rating={rating}
              starRatedColor="blue"
              changeRating={changeRating}
              numberOfStars={5}
              name='rating'
            />
          );
    }
    return(
      <div className="rating-area">
        <ul className="ratings">
          <li className="rate-this">Rate this item:</li>
          {renderRate()}
          <li className="color">(5 votes)</li>
        </ul>
        <ul className="tag">
          <li>TAG:</li>
          <li><a className="color" href>Pink <span>/</span></a></li>
          <li><a className="color" href>T-Shirt <span>/</span></a></li>
          <li><a className="color" href>Girls</a></li>
        </ul>
      </div>
    )
}

export default Ratings;