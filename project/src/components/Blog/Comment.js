import axios from "axios"
import { useState } from "react"
import ErrorLogin from "../../member/ErrorLogin"

function Comment(props) {
    const {id} = props
    const userData = JSON.parse(localStorage.getItem('data'))
    const isLogin = JSON.parse(localStorage.getItem('isLogin'))
    const token = userData.success.token
    const auth = userData.Auth
    const {idComment} = props 
    const [comment,setComment] = useState()
    const [error,setError] = useState({})
    let url = 'http://localhost/laravel/laravel/public/api/blog/comment/' + id
    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    function handleInput(e) {
        setComment(e.target.value)
    }
    function handleSubmit(e) {
       if (comment) {
                const formData = new FormData();
                    formData.append('id_blog',id)
                    formData.append('id_user', auth.id)
                    formData.append('id_comment', idComment ? idComment : 0)
                    formData.append('comment', comment)
                    formData.append('image_user', auth.avatar)
                    formData.append('name_user', auth.name)
        
                    axios.post(url,formData,config)
                    .then(response => {
                        if (response.data.errors) {
                            setError(response.data.errors)
                        } else {
                            props.getCmt([response.data.data])
                        }
                    })
            }
        let errorSubmit ={}
        let flag = true
        e.preventDefault()
        if (!comment) {
            errorSubmit.textarea = 'comment khong duoc de trong' 
            flag = false
        }
        if (!isLogin) {
            alert('vui long dang nhap')
            flag = false
        }
        if (!flag) {
            setError(errorSubmit)
        } else {
            setComment('')
            setError('')
        }
    }

    return(
        <div className="replay-box">
        <div className="row">
          <div>
            <h2>Leave a replay</h2>
            <ErrorLogin error= {error} />
            <form onSubmit={handleSubmit} id="comment" encType="multipart/form-data" method="POST">
              <input type="hidden" name="_token" defaultValue="DV4M0A431dLjdVaCiau52sRa9y0WB36mNexnd4da" />
              <div className="blank-arrow">
                <label>Your Comment</label>
              </div>
              <span>*</span>
              <textarea onChange={handleInput} id="comment" name="comment" rows={11} value = {comment} />
              <input type="hidden" id="id_comment" name="id_comment" defaultValue={0} />
              <button id="submit_form" type="submit" className=" btn btn-primary">post comment</button>
            </form>
          </div>
        </div>
      </div>
    )
};
export default Comment;