import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Blog() {
  const [data,setData] = useState([]);
  useEffect(() =>{
    axios.get('http://localhost/laravel/laravel/public/api/blog')
    .then(res =>{
      // console.log(res)
      setData(res.data.blog.data)
    })
     
      // setData(res.data)
    .catch(error => console.log(error))

  },[])

    function renderBlog() {
      if (data.length > 0) {
        return data.map((value,key) =>{
          return(
            <div key={key} className="single-blog-post">
              <h3>{value.title}</h3>
              <div className="post-meta">
                <ul>
                  <li><i className="fa fa-user" /> Mac Doe</li>
                  <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                  <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                </ul>
                <span>
                  <i className="fa fa-star" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star-half-o" />
                </span>
              </div>
              <a href>
                <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + value.image} alt="" />
              </a>
              <p>{value.description}</p>
              <Link  className="btn btn-primary" to={"/blog/detail/" + value.id}>Read More</Link>
            </div>
            )
        })
      }
    } ;
    return(
        <div className="col-sm-9">
        <div className="blog-post-area">
          <h2 className="title text-center">Latest From our Blog</h2>
          {renderBlog()}
        </div>
      </div>
    )
};
export default Blog;