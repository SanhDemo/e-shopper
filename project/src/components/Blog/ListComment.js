
function ListComment(props) {

    let {comment} = props
    function handleComment() {
        if (comment.length > 0) {
            return (
                comment.map((value,key) => {
                    if (value.id_comment == 0) {
                      return(
                        <>
                        <li key={key} className="media">
                            <a className="pull-left" href="#">
                                <img style={{width: 100 +'px' }} className="media-object" src={"http://localhost/laravel/laravel/public/upload/user/avatar/" + value.image_user} alt="" />
                            </a>
                            <div className="media-body">
                                <ul className="sinlge-post-meta">
                                <li><i className="fa fa-user" />{value.name_user}</li>
                                <li><i className="fa fa-clock-o" />{(value.created_at).split(' ')[1]}</li>
                                <li><i className="fa fa-calendar" />{(value.created_at).split(' ')[0]}</li>
                                </ul>
                                <p>{value.comment}</p>
                                <a onClick={getIdComment} id= {value.id} className="btn btn-primary" href ='#comment'><i className="fa fa-reply" />Replay</a>
                               
                            </div>
                        </li>
                        {
                          comment.map((value1,key1) =>{
                            if (value1.id_comment == value.id) {
                              return(
                                <li key={key1} className="media second-media">
                                  <a className="pull-left" href="#">
                                    <img style={{width: 100 +'px' }} className="media-object" src={"http://localhost/laravel/laravel/public/upload/user/avatar/" + value1.image_user} alt="" />
                                  </a>
                                  <div className="media-body">
                                    <ul className="sinlge-post-meta">
                                      <li><i className="fa fa-user" />{value1.name}</li>
                                      <li><i className="fa fa-clock-o" />{(value1.created_at).split(' ')[1]}</li>
                                      <li><i className="fa fa-calendar" />{(value1.created_at).split(' ')[0]}</li>
                                    </ul>
                                    <p>{value1.comment}</p>
                                    <a onClick={getIdComment} id= {value.id}  className="btn btn-primary" href="#comment"><i className="fa fa-reply" />Replay</a>
                                  </div>
                                </li>
                              )
                            }
                          })
                        }
                        </>
                    )
                    }
                })
            )
        }
    }
    function getIdComment(e) {
      props.getId(e.target.id)
    }
    function deleteReplay() {
      props.getId(0)
    }
    return(
        <div className="response-area">
              <h2>3 RESPONSES</h2>
              <ul className="media-list">
                {handleComment()}
              </ul>
              <button type='submit' onClick={deleteReplay}> xoa replay</button>
            </div>
    )
};
export default ListComment;