import axios from "axios";
import { useEffect, useState,  } from "react";
import { useParams } from "react-router-dom";
import ListComment from "./ListComment";
import Comment from "./Comment";
import Ratings from "./Ratings";

function BlogDetail(props) {
  let params = useParams()
  const [data, setData] = useState([])
  const [listComment, setListComment] = useState([])
  const [idComment ,setIdComment] = useState()
  useEffect(()=>{
    axios.get('http://localhost/laravel/laravel/public/api/blog/detail/' + params.id)
    .then(res =>{
      setListComment(res.data.data.comment)
      setData(res.data.data)})
    },[])
    function getCmt(data) {
        setListComment(listComment.concat(data))
    }
    function getId(id) {
      console.log(id)
      setIdComment(id)
    }
  function renderPostArea() {
    if (Object.keys(data).length > 0 ) {
      return(
            <div className="blog-post-area">
              <h2 className="title text-center">Latest From our Blog</h2>
              <div className="single-blog-post">
                <h3>{data.title}</h3>
                <div className="post-meta">
                  <ul>
                    <li><i className="fa fa-user" /> Mac Doe</li>
                    <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                    <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                  </ul>
                </div>
                <a href>
                  <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + data.image} alt="" />
                </a>
                <p>
                  {data.description}
                </p>
                <div className="pager-area">
                  <ul className="pager pull-right">
                    <li><a href="#">Pre</a></li>
                    <li><a href="#">Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
      )
    }
  }
    return(
        <div className="col-sm-9">
            {renderPostArea()}
            <Ratings id={params.id}/>
            <div className="socials-share">
              <a href><img src="/frontend/images/blog/socials.png" alt="" /></a>
            </div>
            <ListComment getId={getId} comment={listComment}/>
            <Comment idComment={idComment} getCmt={getCmt} id={data.id}/>
        </div>
    )
};
export default BlogDetail;