import axios from "axios";
import { useState } from "react";
import ErrorLogin from "../../member/ErrorLogin";

function Account(props) {
  let data = JSON.parse(localStorage.getItem('data'))
    let url = 'http://localhost/laravel/laravel/public/api/user/update/' + data.Auth.id
    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ data.success.token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    const [inputs , setInputs] = useState(data.Auth);
    const [file,setFile] = useState('')
    const [avatar,setAvatar] = useState('')
    const [error,setError] = useState({})
    function handleChange(e) {
        let valueInput = e.target.value
        console.log(valueInput);
        let nameInput = e.target.name
        setInputs(state => ({...state,[nameInput] : valueInput}))
    }
    function handleFile(e) {
        let file = e.target.files
        if (file) {
            let reader = new FileReader();
            reader.onload = (e) =>{
                setAvatar(e.target.result);
                setFile(file)
            }
            reader.readAsDataURL(file[0])
        }
    }
    function handleTypeFile(files) {
        const arrFile = [
            'png',
            'jpg',
            'jpeg',
            'PNG',
            'JPG'
        ]
        if (files) {
            let nameFile = (files.name).split('.')
            let typeFile = nameFile[nameFile.length - 1]
            return arrFile.includes(typeFile)
        }
    }
    function handleSubmit(e) {
        e.preventDefault()
        let errorSubmit = {};
        let flag = true;
        if(inputs.name === '') {
            errorSubmit.name = 'name: không được để trống'
            flag= false
        }
        if(inputs.phone === '') {
            errorSubmit.phone = 'phone: không được để trống'
            flag= false
        }
        if(inputs.address === '') {
            errorSubmit.address = 'address: không được để trống'
            flag= false
        }
        if (file) {
          if(file[0].size > 1024*1024) {
            errorSubmit.file = 'file: kích thước lớn'
            flag= false
        }
        if(!handleTypeFile(file[0])) {
            errorSubmit.file = 'file: không đúng'
            flag= false
        }
        }
        if(inputs.country === '') {
            errorSubmit.country = 'country: không được để trống'
            flag= false
        }
        if (!flag) {
            setError(errorSubmit)
        } else {
            if (inputs) {
              const formData = new FormData();
                    formData.append('name',inputs.name)
                    formData.append('email',inputs.email)
                    formData.append('password',inputs.password ? inputs.password :'')
                    formData.append('avatar', avatar)
                    formData.append('phone', inputs.phone)
                    formData.append('level', inputs.level)
                    formData.append('address', inputs.address)
                    formData.append('country', inputs.country)
                axios.post(url , formData, config)
                .then(response => {
                  console.log(response.data);
                    if (response.data.errors) {
                        setError(response.data.errors)
                    } else {
                      localStorage.setItem('data',JSON.stringify(response.data))
                        alert('signup thanh cong')
                        setError('')
                    }
                })
            }
        }
    }
    return(
        <div className="col-sm-9">
          <div className="col-md-8">
            <div className="card">
              <div className="card-header">User Update</div>
              <div className="card-body">
                <ErrorLogin error={error}/>
                <form onSubmit={handleSubmit} method="POST" encType="multipart/form-data">
                  <input type="hidden" name="_token" defaultValue="u6l2QYTGF0xsoE5IxodkfmVxKFkFwL1po18MTOac" />
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Full Name (*)</label>
                    <div className="col-md-8">
                    <input className="form-control"  onChange={handleChange} type="name" value={inputs.name} name="name"  />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Email (*)</label>
                    <div className="col-md-8">
                    <input className="form-control" readOnly value={inputs.email} type="email" name="email"  />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Password (*)</label>
                    <div className="col-md-8">
                    <input className="form-control"  onChange={handleChange} type="password" name="password" placeholder='password' />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Phone</label>
                    <div className="col-md-8">
                    <input className="form-control" value={inputs.phone} onChange={handleChange} type="number" name="phone"  />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Address</label>
                    <div className="col-md-8">
                    <input className="form-control" value={inputs.address} onChange={handleChange} type="text" name="address"  />
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Avatar (*)</label>
                    <div className="col-md-8">
                    <input className="form-control"  name="file"  type='file' onChange={handleFile}/>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Country (*)</label>
                    <div className="col-md-8">
                    <select  onChange={handleChange} value={inputs.country} name="country" className="form-control form-control-line">
                        <option value>Please select</option>
                        <option value={1}>vietnam</option>
                        <option value={2}> anh</option>
                        <option value={3}>phap</option>
                      </select>
                    </div>
                  </div>
                  <div className="form-group row mb-0">
                    <div className="col-md-8 offset-md-4">
                    <button type="submit" className="btn btn-default">Signup</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
      
        </div>
    )
}
export default Account;