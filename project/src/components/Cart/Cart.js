import axios from "axios";
import { useEffect, useState ,useContext} from "react";
import { UserContext } from "../../UserContext";

function Cart(props) {
  const {setSumQty} = useContext(UserContext)
  let dataProduct = JSON.parse(localStorage.getItem('dataProduct'))
  localStorage.setItem('dataAddProduct',JSON.stringify(dataProduct))
  const [data,setData] = useState([])
   useEffect(()=>{
     axios.post('http://localhost/laravel/laravel/public/api/product/cart', dataProduct)
    .then(res=> {
        setData(res.data.data);
    })
   },[])
   function handleCart() {
    if(data.length > 0) {
      return data.map((value,key)=>{
        let img = JSON.parse(value.image)
        return (
                  <tr key={key}>
                        <td className="cart_product">
                          <a href><img style={{'width' : '100px', 'height':"71px"}} src={"http://localhost/laravel/laravel/public/upload/user/product/" + value.id_user + '/' + img[0]} alt="" /></a>
                        </td>
                        <td className="cart_description">
                          <h4><a href>{value.detail}</a></h4>
                          <p>Web ID: {value.wep_id}</p>
                        </td>
                        <td className="cart_price">
                          <p>${value.price}</p>
                        </td>
                        <td className="cart_quantity">
                          <div className="cart_quantity_button">
                            <a style={{'cursor':'pointer'}} id={value.id} onClick={handleAscending} className="cart_quantity_up" href> + </a>
                            <input className="cart_quantity_input" type="text" name="quantity" defaultValue='1' value={value.qty} autoComplete="off" size={2} />
                            <a style={{'cursor':'pointer'}} id={value.id} onClick={handleDecrease} className="cart_quantity_down" href> - </a>
                          </div>
                        </td>
                        <td className="cart_total">
                          <p className="cart_total_price">${value.price * value.qty}</p>
                        </td>
                        <td className="cart_delete">
                          <a style={{'cursor':'pointer','padding':'none'}} className="cart_quantity_delete" href><i style={{'cursor':'pointer','padding':'7px 5px'}} id={value.id} onClick={handleDeleteCart} className="fa fa-times" /></a>
                        </td>
                  </tr>
              )
      })
    }
   }
   let sum = 0
   data.map((value,key)=>{
    sum += value.qty
  })
  setSumQty(sum)
  //  ham tang san pham
   function handleAscending(e) {
    let id = e.target.id
    let newData = [...data]
     newData.map((value,key)=> {
        if (id == value.id) {
        return  value.qty ++
        }
      })
      setData(newData)
    Object.keys(dataProduct).map((key,index)=> {
      if (id == key) {
        return dataProduct[key] ++
      } 
    })
    localStorage.setItem('dataProduct',JSON.stringify(dataProduct))
  }
  // ham giam san pham
  function handleDecrease(e) {
    let id = e.target.id
   let newData = [...data]
   newData.map((value,key)=> {
      console.log(value);
      if (id == value.id) {
        if (value.qty > 1) {
          return  value.qty --
        } else {
          return newData.splice(key,1)
        }
      }
      })
      setData(newData)
      Object.keys(dataProduct).map((key,index)=> {
        if (id == key) {
          if (dataProduct[key] > 1){
            return dataProduct[key] --
          } else {
            delete dataProduct[key]
          }
        } 
      })
      localStorage.setItem('dataProduct',JSON.stringify(dataProduct))
  }
  // ham xoa san pham
  const handleDeleteCart =(e) => {
    let id = e.target.id
    let  newData = [...data]
    newData.map((value,key)=>{
      if (id == value.id) {
        newData.splice(key,1)
      }
    })
    setData(newData)
    Object.keys(dataProduct).map((key,index)=> {
      if (id == key) {
        delete dataProduct[key]
      } 
    })
    localStorage.setItem('dataProduct',JSON.stringify(dataProduct))
  }
  // thanh toán
  function CartSub() {
    let sumPrice = 0;
    if (data.length > 0) {
      data.map((value,key) =>{
        sumPrice += (value.price * value.qty)
        return sumPrice
      })
    }
    return( 
            <ul>
              <li>Cart Sub Total <span>${sumPrice}</span></li>
              <li>Eco Tax <span>$2</span></li>
              <li>Shipping Cost <span>Free</span></li>
              <li>Total <span>${sumPrice + 2}</span></li>
            </ul>
    )
  }
    return(
      <div className="col-sm-9 ms-sm-1">
        <section  id="cart_items">
          <div className="container">
            <div className="breadcrumbs">
              <ol className="breadcrumb">
                <li><a href="#">Home</a></li>
                <li className="active">Shopping Cart</li>
              </ol>
            </div>
            <div style={{'padding':'0'}} className=" col-sm-9 px-0 table-responsive cart_info">
              <table className=" table table-condensed">
                <thead>
                  <tr className="cart_menu">
                    <td className="image">Item</td>
                    <td className="description" />
                    <td className="price">Price</td>
                    <td className="quantity">Quantity</td>
                    <td className="total">Total</td>
                    <td />
                  </tr>
                </thead>
                <tbody>
                  {handleCart()}
                </tbody>
              </table>
            </div>
          </div>
        </section> {/*/#cart_items*/}
        <section className="col-sm-9 " id="do_action">
          <div className="container">
            <div className="heading">
              <h3>What would you like to do next?</h3>
              <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
            </div>
            <div className="row">
              <div className="col-sm-4">
                <div className="chose_area">
                  <ul className="user_option">
                    <li>
                      <input type="checkbox" />
                      <label>Use Coupon Code</label>
                    </li>
                    <li>
                      <input type="checkbox" />
                      <label>Use Gift Voucher</label>
                    </li>
                    <li>
                      <input type="checkbox" />
                      <label>Estimate Shipping &amp; Taxes</label>
                    </li>
                  </ul>
                  <ul className="user_info">
                    <li className="single_field">
                      <label>Country:</label>
                      <select>
                        <option>United States</option>
                        <option>Bangladesh</option>
                        <option>UK</option>
                        <option>India</option>
                        <option>Pakistan</option>
                        <option>Ucrane</option>
                        <option>Canada</option>
                        <option>Dubai</option>
                      </select>
                    </li>
                    <li className="single_field">
                      <label>Region / State:</label>
                      <select>
                        <option>Select</option>
                        <option>Dhaka</option>
                        <option>London</option>
                        <option>Dillih</option>
                        <option>Lahore</option>
                        <option>Alaska</option>
                        <option>Canada</option>
                        <option>Dubai</option>
                      </select>
                    </li>
                    <li className="single_field zip-field">
                      <label>Zip Code:</label>
                      <input type="text" />
                    </li>
                  </ul>
                  <a className="btn btn-default update" href>Get Quotes</a>
                  <a className="btn btn-default check_out" href>Continue</a>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="total_area">
                  {CartSub()}
                  <a className="btn btn-default update" href>Update</a>
                  <a className="btn btn-default check_out" href>Check Out</a>
                </div>
              </div>
            </div>
          </div>
        </section></div>
    )
}
export default Cart;