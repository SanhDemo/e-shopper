import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import React from "react";
import { UserContext } from "../../UserContext";
function Home(props) {
  const {setSumQty} = useContext(UserContext)
  let dataAddProduct = JSON.parse(localStorage.getItem('dataAddProduct'))
  let wishlist = JSON.parse(localStorage.getItem('wishlist'))
  let isLogin = JSON.parse(localStorage.getItem('isLogin'))
  let [dataWishlist,setDataWishlist] = useState([])
    const [ data, setData] = useState([])
    const [objProduct,setObjProduct] = useState({})
    useEffect(()=> {
      axios.get('http://localhost/laravel/laravel/public/api/product')
      .then(res => {
        setData(res.data.data);
      })
      if(dataAddProduct) {
        setObjProduct({...objProduct, ...dataAddProduct})
      }
      if(wishlist) {
        setDataWishlist(dataWishlist.concat(wishlist))
      }
    },[])
    function renderProductHome() {
      if (data) {
        return Object.keys(data).map((key,index)=>{
        let img = JSON.parse(data[key].image)
          return (
            <div className="col-sm-4">
                  <div className="product-image-wrapper">
                    <div className="single-products">
                      <div className="productinfo text-center">
                        <img src={'http://localhost/laravel/laravel/public/upload/user/product/'+ data[key].id_user +'/'+ img[0]} alt="" />
                        <h2>${data[key].price}</h2>
                        <p>{data[key].name}</p>
                        <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                      </div>
                      <div className="product-overlay">
                        <div className="overlay-content">
                          <h2>${data[key].price}</h2>
                          <p>{data[key].name}</p>
                          <a href="#" id={data[key].id} onClick={handelAddProduct}  className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                        </div>
                      </div>
                    </div>
                    <div className="choose">
                      <ul className="nav nav-pills nav-justified">
                        <li><a id={data[key].id} onClick={handleWislist} href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                        <li><Link to ={'/product/detail/'+ data[key].id}><i className="fa fa-plus-square" />Product detail</Link></li>
                      </ul>
                    </div>
                  </div>
                </div>
          )
        })
      }
    }
    function handleWislist(e) {
      let id = e.target.id
      let check = dataWishlist.includes(id)
      if (!check) {
        setDataWishlist(state => ([...state,e.target.id]))
      } 
    }
    console.log(objProduct);
    localStorage.setItem('dataWishlist',JSON.stringify(dataWishlist))
    // ham them san pham
    const handelAddProduct = (e) =>{
      let id = e.target.id
      if (isLogin) {
      let a = Object.keys(objProduct).includes(id)
          if(a) {
            setObjProduct(state =>({...state,[id]:objProduct[id] + 1}))
          } else {
              setObjProduct(state =>({...state,[id]:1}))
            }
      } else {
        alert('vui long dang nhap')
      }
      Qty()
    }
    // tinh tong so hang trang cos trong gio
    function Qty() {
      let sum = 0;
      Object.keys(objProduct).map((key,index)=>{
        sum += objProduct[key]
      })
      setSumQty(sum)
    }
    localStorage.setItem('dataProduct',JSON.stringify(objProduct))
    return (
        <div className="col-sm-9 padding-right">
              <div className="features_items">{/*features_items*/}
                <h2 className="title text-center">Features Items</h2>
                {renderProductHome()}
              </div>{/*features_items*/}
            </div>
    )
};
export default Home;