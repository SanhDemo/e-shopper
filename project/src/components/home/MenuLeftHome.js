function MenuLeftHome(props) {
    return(
        <div className="col-sm-3">
        <div className="left-sidebar">
          <h2>Category</h2>
          <div className="panel-group category-products" id="accordian">{/*category-productsr*/}
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title"><a href="http://localhost/laravel/laravel/public/product/category/1">Category1</a></h4>
              </div>
            </div>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title"><a href="http://localhost/laravel/laravel/public/product/category/2">Category2</a></h4>
              </div>
            </div>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h4 className="panel-title"><a href="http://localhost/laravel/laravel/public/product/category/3">vietnam</a></h4>
              </div>
            </div>
          </div>{/*/category-products*/}
          <div className="brands_products">{/*brands_products*/}
            <h2>Brands</h2>
            <div className="brands-name">
              <ul className="nav nav-pills nav-stacked">
                <li><a href="http://localhost/laravel/laravel/public/product/brand/1">Brand1</a></li>
                <li><a href="http://localhost/laravel/laravel/public/product/brand/2">Brand2</a></li>
              </ul>
            </div>
          </div>{/*/brands_products*/}
          <div className="shipping text-center">{/*shipping*/}
            <img src="http://localhost/laravel/laravel/public/frontend/images/home/shipping.jpg" alt="" />
          </div>{/*/shipping*/}
        </div>
      </div>
    )
};
export default MenuLeftHome;