import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ErrorLogin from "../../member/ErrorLogin";
function AddProduct(props) {
    const navigate = useNavigate()
    let dataUser = JSON.parse(localStorage.getItem('data'))
    let token = dataUser.success.token
    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    const [inputs, setInput] = useState({
        category:'',
        brand:'',
        name:'',
        price:'',
        status:1,
        sale:0,
        detail:'',
        company:''
    })
    const [ error, setError] = useState({})
    const [ data , setData] = useState([])
    const [file,setFile] = useState('')
    useEffect(()=> {
        axios.get('http://localhost/laravel/laravel/public/api/category-brand')
        .then(res=> {
            setData(res.data);
        })
    },[])
    // ham hien input sale 
    function renderSale(status) {
        if(status == 0) {
            return(
                <div>
                    <input className="form-control form-control-line" style={{"width":"20%",'display': "inline-block"}} onChange={handleChange} type="text" name="sale" placeholder="Sale" />
                    <label style={{"margin-left":"10px",'font-size': "16px"}}>%</label>
                </div>
            )
        }
    }
    //  ham render ra category va brand
    function renderSelect(data,name) {
        function render() {
            if (name == 'category') {
                let category = data.category
                if (category) {
                    return category.map((value,key) => {
                        return <option key={key} value={value.id}>{value.category}</option>
                    })
                }
            }
            if (name == 'brand') {
                let brand = data.brand
                if (brand) {
                    return brand.map((value,key) => {
                        return <option key={key} value={value.id}>{value.brand}</option>
                    })
                }
            }
        }
        return(
            <div >
                <select  onChange={handleChange}  name={name} className="form-control form-control-line">
                    <option selected value=''>Please choose {name}</option>
                    {render()}
                </select>
            </div>
        )
    }
    function handleChange(e) {
        let valueInput = e.target.value
        let nameInput = e.target.name
        setInput(state => ({...state,[nameInput]:valueInput}));
    }
    function handleFile(e) {
        setFile(e.target.files)
    }
    function handleSubmit(e) {
        e.preventDefault()
        let errorSubmit = {};
        let flag = true;
        function handleCheckFile () {
            console.log(flag);
            const arrType = [
                'png',
                 'jpg',
                 'jpeg',
                 'PNG',
                 'JPG'
            ]
            if (file.length <= 3) {
                Object.keys(file).map((key,index)=>{
                    let nameFile = file[key].name
                    let typeFile = nameFile.split('.')[1]
                    console.log(typeFile);
                    if (file[key].size > 1024*1024) {
                        errorSubmit.file = 'file: kich thuoc lon'
                        flag = false
                    }
                    if (!arrType.includes(typeFile)) {
                        errorSubmit.file = 'file: khong dung'
                        flag = false
                    }
                })
            } else {
                errorSubmit.file = 'file: qua so luong cho phep'
                        flag = false
            }
        }
        if(inputs.category === '') {
            errorSubmit.category = 'category: không được để trống'
            flag= false
        }
        if(inputs.brand === '') {
            errorSubmit.brand = 'brand: không được để trống'
            flag= false
        }
        if(inputs.name === '') {
            errorSubmit.name = 'name: không được để trống'
            flag= false
        }
        if(inputs.price === '') {
            errorSubmit.price = 'price: không được để trống'
            flag= false
        }
        if(inputs.status === '') {
            errorSubmit.status = 'status: không được để trống'
            flag= false
        } else {
            if(inputs.sale === '' &&  inputs.status == 0) {
                errorSubmit.sale = 'sale: không được để trống'
                flag= false
            }
        }
        if (file === '' ) {
                errorSubmit.file = 'file: không được để trống'
                flag= false
        } else {
            {handleCheckFile()}
        }
        if(inputs.company === '') {
            errorSubmit.company = 'company: không được để trống'
            flag= false
        }
       
        if(inputs.detail === '') {
            errorSubmit.detail = 'detail: không được để trống'
            flag= false
        }
        if (!flag) {
            setError(errorSubmit)
        } else {
            const formData = new FormData()
                formData.append("name",inputs.name)
                formData.append("price",inputs.price)
                formData.append("category",inputs.category)
                formData.append("brand",inputs.brand)
                formData.append("company",inputs.company)
                formData.append("detail",inputs.detail)
                formData.append("status",inputs.status)
                formData.append("sale",inputs.sale ? inputs.sale : '')
                Object.keys(file).map((key,index) => {
                    formData.append('file[]',file[key])
                })
            axios.post('http://localhost/laravel/laravel/public/api/user/add-product',formData, config)
            .then(response => {
                // console.log(response.data);
                if (response.data.errors) {
                    setError(response.data.errors)
                } else {
                    setError('')
                    navigate('/account/product')
                }
            })
        }
    }

    return(
        <section className="col-sm-9 ms-sm-1" >
                <div class="row">
                    <div className="signup-form">{/*sign up form*/}
                        <h2>Create product!</h2>
                        <ErrorLogin error={error}/>
                        <form onSubmit={handleSubmit} action="#" enctype='multipart/form-data'>
                            <input className="form-control" onChange={handleChange} type="name" name="name" placeholder="Name" />
                            <input className="form-control" onChange={handleChange}  type="text" name="price" placeholder="Price" />
                            {renderSelect(data,'category')}
                            {renderSelect(data,'brand')}
                            <div >
                                <select  onChange={handleChange}  name="status" className="form-control form-control-line">
                                    <option selected value=''>Status</option>
                                    <option  value={1}>new</option>
                                    <option value={0}>sale </option>
                                </select>
                            </div>
                            {renderSale(inputs.status)}
                            <input className="form-control" onChange={handleChange} type="text" name="company" placeholder="Company profile" />
                            <input className="form-control" name="file" multiple type='file' onChange={handleFile}/>
                            <textarea className="form-control" onChange={handleChange} type="text" name="detail" placeholder="Detail"></textarea>
                            <button type="submit" className="btn btn-default">signup</button>
                        </form>
                    </div>
                </div>
        </section>
    )
};
export default AddProduct;