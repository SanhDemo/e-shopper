import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import ErrorLogin from "../../member/ErrorLogin";
function EditProduct(props) {
    const params = useParams()
    const [avatarCheckBox, setAvatarCheckBox] = useState([])
    const [ error, setError] = useState({})
    const [ data , setData] = useState([])
    const [inputs, setInput] = useState({})
    const [file,setFile] = useState('')
    const navigate = useNavigate()
    let dataUser = JSON.parse(localStorage.getItem('data'))
    let token = dataUser.success.token
    let id = dataUser.Auth.id
    let url = 'http://localhost/laravel/laravel/public/api/user/edit-product/' + params.id
    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    // lay data product
    useEffect(()=> {
        axios.get('http://localhost/laravel/laravel/public/api/user/product/'+ params.id,config)
        .then(res=> {
            setInput(res.data.data);
        })
        .catch(error => console.log(error))
    },[])
    // lay du lieu category va brand
    useEffect(()=> {
        axios.get('http://localhost/laravel/laravel/public/api/category-brand')
        .then(res=> {
            setData(res.data);
        })
    },[])
    function renderSale() {
        if(inputs.status == 0) {
            return(
                <div>
                    <input value={inputs.sale} className="form-control form-control-line" style={{"width":"20%",'display': "inline-block"}} onChange={handleChange} type="text" name="sale" placeholder="Sale" />
                    <label style={{"margin-left":"10px",'fontSize': "16px"}}>%</label>
                </div>
            )
        }
    }
    //  ham render ra category va brand
    function renderSelect(name) {
        function render() {
            if (name == 'category') {
                let category = data.category
                if (category) {
                    return category.map((value,key) => {
                        return <option key={key} value={value.id}>{value.category}</option>
                    })
                }
            }
            if (name == 'brand') {
                let brand = data.brand
                if (brand) {
                    return brand.map((value,key) => {
                        return <option key={key} value={value.id}>{value.brand}</option>
                    })
                }
            }
        }
        return(
            <div >
                <select onChange={handleChange} value={inputs['id_'+ name]} name={'id_'+name } className="form-control form-control-line">
                    <option selected value=''>Please choose {name}</option>
                    {render()}
                </select>
            </div>
        )
    }
    // ham xoa hinh anh
    function renderCheckImg() {
        let arrImg = inputs.image
        function renderImg() {
            if (arrImg) {
                return  arrImg.map((value,key)=>{
                      return(
                        <div>
                          <img style={{'width':'100px','height':"70px"}} src={'http://localhost/laravel/laravel/public/upload/user/product/'+ id +'/'+ value} alt="" />
                          <input onClick={checkboxImg} id={key} name={value} type='checkbox' />
                        </div>
                      )
                  })
              }
        }
        function checkboxImg(e) {
            if (e.target.checked) {
            //   let imgRemaining = inputs.image.splice(e.target.id,1)
                // console.log(imgRemaining);
                setAvatarCheckBox(state=>([...state,e.target.name]))
            } else {
              let arr =  avatarCheckBox.filter((value) => {
                    return value != e.target.name
                })
                setAvatarCheckBox(arr)
            }
        }
        return(
            <div style={{'display':'flex','justify-content':"space-around"}}>
                {renderImg()}
            </div>
        )
    }

    function handleChange(e) {
        let valueInput = e.target.value
        let nameInput = e.target.name
        setInput(state => ({...state,[nameInput]:valueInput}));
    }
    function handleFile(e) {
        setFile(e.target.files)
    }

    function handleSubmit(e) {
        console.log(inputs.id);
        
        e.preventDefault()
        // check file
        function handleCheckFile () {
            console.log(file);
            const arrType = [
                'png',
                 'jpg',
                 'jpeg',
                 'PNG',
                 'JPG'
            ]
            // tong so file khi update
           let sumFile = (file.length)+((inputs.image.length)-(avatarCheckBox.length));
           console.log(sumFile);
            if (sumFile <= 3) {
                Object.keys(file).map((key,index)=>{
                    let nameFile = file[key].name
                    let typeFile = nameFile.split('.')[1]
                    if (file[key].size > 1024*1024) {
                        errorSubmit.file = 'file: kich thuoc lon'
                        flag = false
                    }
                    if (!arrType.includes(typeFile)) {
                        errorSubmit.file = 'file: khong dung'
                        flag = false
                    }
                })
            } else {
                errorSubmit.file = 'file: qua so luong cho phep'
                        flag = false
            }
        }
        let errorSubmit = {};
        let flag = true;
        if(inputs.id_category === '') {
            errorSubmit.category = 'category: không được để trống'
            flag= false
        }
        if(inputs.id_brand === '') {
            errorSubmit.brand = 'brand: không được để trống'
            flag= false
        }
        if(inputs.name === '') {
            errorSubmit.name = 'name: không được để trống'
            flag= false
        }
        if(inputs.price === '') {
            errorSubmit.price = 'price: không được để trống'
            flag= false
        }
        if(inputs.status === '') {
            errorSubmit.status = 'status: không được để trống'
            flag= false
        } else {
            if(inputs.sale === '' &&  inputs.status == 0) {
                errorSubmit.sale = 'sale: không được để trống'
                flag= false
            }
        }
        if (file === '' ) {
                errorSubmit.file = 'file: không được để trống'
                flag= false
        } else {
            handleCheckFile()
        
        }
        if(inputs.company_profile === '') {
            errorSubmit.company = 'company: không được để trống'
            flag= false
        }
       
        if(inputs.detail === '') {
            errorSubmit.detail = 'detail: không được để trống'
            flag= false
        }
        if (!flag) {
            setError(errorSubmit)
        } else {
            const formData = new FormData()
                formData.append("name",inputs.name)
                formData.append("price",inputs.price)
                formData.append("category",inputs.id_category)
                formData.append("brand",inputs.id_brand)
                formData.append("company",inputs.company_profile)
                formData.append("detail",inputs.detail)
                formData.append("status",inputs.status)
                formData.append("sale",inputs.sale ? inputs.sale : '')
                Object.keys(file).map((key,index) => {
                    formData.append('file[]',file[key])
                })
                Object.keys(avatarCheckBox).map((key,index) => {
                    formData.append('avatarCheckBox[]',avatarCheckBox[key])
                })
            axios.post(url,formData, config)
            .then(response => {
                console.log(response.data.success);
                if (response.data.errors) {
                    setError(response.data.errors)
                } else {
                    setError('')
                    navigate('/account/product')
                }
            })
        }
    }
    return(
        <section className="col-sm-9 ms-sm-1" >
                <div class="row">
                    <div className="signup-form">{/*sign up form*/}
                        <h2>Create product!</h2>
                        <ErrorLogin error={error}/>
                        <form onSubmit={handleSubmit} action="#" enctype='multipart/form-data'>
                            <input className="form-control" onChange={handleChange} value={inputs.name} type="name" name="name" placeholder="Name" />
                            <input className="form-control" onChange={handleChange} value={inputs.price} type="text" name="price" placeholder="Price" />
                            {renderSelect('category')}
                            {renderSelect('brand')}
                            <div >
                                <select id={inputs.status} onChange={handleChange} value={inputs.status} name="status" className="form-control form-control-line">
                                 
                                    <option  value={1}>new</option>
                                    <option value={0}>sale </option>
                                </select>
                            </div>
                            {renderSale()}
                            <input className="form-control" onChange={handleChange} value={inputs.company_profile} type="text" name="company_profile" placeholder="Company profile" />
                            <input className="form-control" name="file" multiple type='file' onChange={handleFile}/>
                            {renderCheckImg()}
                            <textarea value={inputs.detail} className="form-control" onChange={handleChange} type="text" name="detail" placeholder="Detail"></textarea>
                            <button  type="submit" className="btn btn-default">signup</button>
                        </form>
                    </div>
                </div>
        </section>
    )
};
export default EditProduct;