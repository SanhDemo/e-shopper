import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function AccountProduct(props) {
    let [data,setData] = useState()
    let dataUser = JSON.parse(localStorage.getItem('data'))
    let id = dataUser.Auth.id
    let token = dataUser.success.token
    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    useEffect(()=>{
        axios.get('http://localhost/laravel/laravel/public/api/user/my-product',config)
        .then(res=>{
            setData(res.data.data);
        })
        .catch(error => console.log(error))
    },[])
    function deleteProduct(e) {
       let id = e.target.id
       axios.get('http://localhost/laravel/laravel/public/api/user/delete-product/' + id ,config)
       .then(res=> {
        setData(res.data.data);
       })
    }
    function handleMyProduct() {
        if (data) {
            return Object.keys(data).map((key, index)=>{
                let img = JSON.parse(data[key].image)
                return(
                    <tr key={index}>
                        <td>{data[key].id}</td>
                        <td>{data[key].name}</td>
                        <td><img style={{"width": '100px','height':'70px'}} src={'http://localhost/laravel/laravel/public/upload/user/product/'+id +'/'+ img[0]} alt="" /></td>
                        <td style={{color:'black'}}>${data[key].price}</td>
                        <td style={{'cursor':'pointer'}}>
                            <Link  to={'/account/editProduct/' + data[key].id}>sửa</Link>
                        </td>
                        <td style={{'cursor':'pointer'}} >
                            <a href="#" id={data[key].id} onClick ={deleteProduct}>xóa</a>
                        </td>
                    </tr>
                )
            })
        }
    }
    return(
        <div className="col-sm-9">
            <table cellPadding={10} className=" my-product">
                <thead className="my-product__header">
                    <tr><th>Id</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th colSpan={2}>Action</th>
                    </tr>
                </thead>
                <tbody className="my-product__body">
                    {handleMyProduct()}
                </tbody>
            </table>
      </div>
    )
};
export default AccountProduct;