import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
function DetailProductZoom(props) {
  const [show, setShow] = useState(false);
  const handleModal =(e) =>{
    e.preventDefault()  
    setShow(!show)
  }
  return (
    <>
      <Button style={{'margin-top':'0'}} variant="primary" onClick={handleModal}>
        Zoom
      </Button>
      <Modal
        show={show}
        onHide={handleModal}
        animation={false}
        style = {{'background-color':"#ECE9C3"}}
      >
        <Modal.Header >
          <Button
            variant="secondary"
            onClick={handleModal}
            className=" cursor-pointer  "
          >
            Close
          </Button>
        </Modal.Header>
        
        <Modal.Body >
          <img style={{'width':'100%'}} src={props.src} alt="" className=" w-full object-cover" />
        </Modal.Body>
      </Modal>
      
    </>
  );
}
export default DetailProductZoom;