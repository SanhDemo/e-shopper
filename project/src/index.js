import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router , Routes, Route } from 'react-router-dom';
import Blog from './components/Blog/Blog';
import BlogDetail from './components/Blog/BlogDetail'
import Index from './member/Index';
import Home from './components/home/Home';
import Account from './components/Account/Account';
import AccountProduct from './components/product/AccountProduct';
import AddProduct from './components/product/AddProduct';
import EditProduct from './components/product/EditProduct';
import ProductDetail from './components/product/ProductDetail';
import Cart from './components/Cart/Cart';
import Wishlist from './components/wishlist/Wishlist';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
    <App >
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route  path='/blog/list' element={<Blog/>} />
        <Route path='/blog/detail/:id' element={<BlogDetail/>} />
        <Route path='/login' element={<Index/>} />
        <Route index path='/home' element={<Home/>} />
        <Route path='/account' element={<Account/>}/>
        <Route path='/account/product' element={<AccountProduct/>} />
        <Route path='/account/addProduct' element={<AddProduct/>} />
        <Route path='/account/editProduct/:id' element ={<EditProduct/>} />
        <Route path='/product/detail/:id' element={<ProductDetail/>}/>
        <Route path='/cart' element={<Cart/>} />
        <Route path='/wishlist' element= {<Wishlist/>}/>
      </Routes>
    </App>

    </Router>
    {/* <Header/> */}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
